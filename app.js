const express = require('express');
const authRoutes = require('./routes/auth-routes');
const dashBoardRoutes = require('./routes/dashboard-routes');
const passportSetup = require('./config/passport-config');
const mongoose = require('mongoose');
const keys = require('./config/keys');
const cookieSession = require('cookie-session');
const passport = require('passport')

const app = express();
//set-up view engine
app.set('view engine', 'ejs');

// use cookies for autherization.
app.use(cookieSession({
  maxAge: 24 * 60 * 60 * 1000,
  keys: [keys.session.keys]
}));

// use passport as middleware
app.use(passport.initialize());
app.use(passport.session());


//connect to DB
mongoose.connect(keys.DB.URI, () => {
  console.log('connected to mongoDB');

});

//create home route
app.get('/', (req, res) => {
  res.render('home');
})

app.use('/auth', authRoutes);
app.use('/dashboard', dashBoardRoutes);

app.listen(3000, () => {
  console.log("listening to 3000");
});